# Upgrade Procedure
1. `docker-compose down`
2. `docker image prune -a`
3. `docker-compose up -d`

This is required because if you do not delete the existing scrutiny image, 
running `docker-compose up -d` will use the existing image.
